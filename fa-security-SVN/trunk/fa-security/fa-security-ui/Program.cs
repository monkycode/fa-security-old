﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using TezaUtils.Model;
using TezaUtils.Forms;

namespace fa_security_ui
{
 
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Login());
            if (UsuarioActivo.getInstance().getUsuario() != null)
            {
                Application.Run(new FrmMain());
            }
        }
    }
}
