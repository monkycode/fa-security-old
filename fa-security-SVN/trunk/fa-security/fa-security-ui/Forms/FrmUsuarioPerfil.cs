﻿using fa_security_ui.Dao;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TezaUtils.Model;

namespace fa_security_ui.Forms
{
    public partial class FrmUsuarioPerfil : Form
    {
        private PerfilUsuario pu;

        public FrmUsuarioPerfil(PerfilUsuario pu)
        {
            InitializeComponent();
            this.pu = pu;

            this.textBox1.Text = pu.C15_USUARIO;
            this.textBox2.Text = pu.NRO_ITEM.ToString();
            this.textBox3.Text = pu.DES_PROGRAMA;
            this.textBox4.Text = pu.DES_MENU;

            this.cbAlta.Checked = (pu.EST_ALTA.Equals("T"));
            this.cbAux1.Checked = (pu.EST_AUX1.Equals("T"));
            this.cbAux2.Checked = (pu.EST_AUX2.Equals("T"));
            this.cbBaja.Checked = (pu.EST_BAJA.Equals("T"));
            this.cbMenu.Checked = (pu.EST_MENU.Equals("T"));
            this.cbModifica.Checked = (pu.EST_MODIFICA.Equals("T"));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.cbAlta.Checked)
            {
                pu.EST_ALTA = "T";
            }
            else
            {
                pu.EST_ALTA = "F";
            }

            if (this.cbAux1.Checked)
            {
                pu.EST_AUX1 = "T";
            }
            else
            {
                pu.EST_AUX1 = "F";
            }

            if (this.cbAux2.Checked)
            {
                pu.EST_AUX2 = "T";
            }
            else
            {
                pu.EST_AUX2 = "F";
            }

            if (this.cbBaja.Checked)
            {
                pu.EST_BAJA = "T";
            }
            else
            {
                pu.EST_BAJA = "F";
            }

            if (this.cbMenu.Checked)
            {
                pu.EST_MENU = "T";
            }
            else
            {
                pu.EST_MENU = "F";
            }

            if (this.cbModifica.Checked)
            {
                pu.EST_MODIFICA = "T";
            }
            else
            {
                pu.EST_MODIFICA = "F";
            }

            var dao = new UsuarioPerfilDao();
            dao.ActualizarUsuarioPerfil(this.pu);
            MessageBox.Show("Permiso actualizado con Exito");
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
