﻿using fa_security_ui.Dao;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TezaUtils.Dao;
using TezaUtils.Forms;
using TezaUtils.Helper;
using TezaUtils.Model;

namespace fa_security_ui.Forms
{
    public partial class FrmUsuarioPermisos : Form
    {
        private Usuario usuario;

        private PerfilDao pdao;

        private UsuarioPerfilDao updao;

        public List<PerfilUsuario> list { get; set; }

        private BackgroundWorker bw;

        private FrmLoading frmLoading = new FrmLoading();

        public FrmUsuarioPermisos(Usuario usuario)
        {
            InitializeComponent();

            this.usuario = usuario;
            this.pdao = new PerfilDao();
            this.updao = new UsuarioPerfilDao();

            this.bw = new BackgroundWorker();
            
            // Configuracion de los eventos de bw, en este caso configuro el completed porque el dowork depende de la solapa
            bw.RunWorkerCompleted += BwRunWorkerCompleted;
            // asigno el delegate correspondiente
            bw.DoWork += BwDoWorkActualizarPerfil;
            // asgino metodo para informar cambios de estado
            bw.ProgressChanged += BwProgressChanged;

            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = true;


            DisableFilter();
            
            // busca la data de la bd y la guarda en la lista
            RefreshData();
            
            // el rb sin filtro muestra la lista en la grilla
            rbSinFiltro.Checked = true;
        }

        private void BwProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void BwDoWorkActualizarPerfil(object sender, DoWorkEventArgs e)
        {
            this.updao.ActualizarPermisosFaltantes(this.usuario);
        }

        private void BwRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.frmLoading.Hide();
            MessageBox.Show("Perfil actualizado con Exito");
            RefreshData();
            ShowListData(this.list);
        }

        private void RefreshData()
        {
            InitializeListView();
            this.textBox1.Text = this.usuario.C15_USUARIO;
            this.textBox2.Text = this.usuario.DES_USUARIO;
            this.textBox3.Text = this.usuario.COD_PERFIL;
            this.list = pdao.GetPerfilUsuario(this.usuario); 
            this.cbPrograma.Items.Clear();
            this.cbPrograma.Items.AddRange(updao.GetProgramas(this.usuario).ToArray());
        }

        private void ShowListData(List<PerfilUsuario> list)
        {
            lvItems.BeginUpdate();
            lvItems.Items.Clear();

            var items = new ListViewItem[list.Count];
            var itemNro = 0;

            foreach (var es in list)
            {
                var item = CreateItemTrxlv(es);

                items[itemNro++] = item;
            }
            lvItems.Items.AddRange(items);
            lvItems.EndUpdate();
            toolStripStatusLabel2.Text = list.Count().ToString();
            this.statusStrip1.Refresh();
        }

        private ListViewItem CreateItemTrxlv(PerfilUsuario pu)
        {
            string[] row = { pu.DES_MENU, 
                             pu.DES_PROGRAMA, 
                             pu.EST_ALTA,
                             pu.EST_AUX1,
                             pu.EST_AUX2,
                             pu.EST_BAJA,
                             pu.EST_MENU,
                             pu.EST_MODIFICA,
                             pu.NRO_ITEM.ToString()
                           };
            ListViewItem item = new ListViewItem(row);

            item.Tag = pu;
            return item;
        }

        private void InitializeListView()
        {
            lvItems.Columns.Clear();
            lvItems.Columns.Add("DES_MENU");
            lvItems.Columns.Add("DES_PROGRAMA");
            lvItems.Columns.Add("EST_ALTA");
            lvItems.Columns.Add("EST_AUX1");
            lvItems.Columns.Add("EST_AUX2");
            lvItems.Columns.Add("EST_BAJA");
            lvItems.Columns.Add("EST_MENU");
            lvItems.Columns.Add("EST_MODIFICA");
            lvItems.Columns.Add("NRO_ITEM");
        }

        private void rbFiltroItem_CheckedChanged(object sender, EventArgs e)
        {
            this.cbPrograma.Enabled = false;
            this.txtNroItemFilter.Enabled = true;
            this.button1.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var filterList = this.list.Where(pu => pu.NRO_ITEM.ToString().Contains(txtNroItemFilter.Text.Trim()));
            ShowListData(filterList.ToList<PerfilUsuario>());
        }

        private void rbSinFiltro_CheckedChanged(object sender, EventArgs e)
        {
            DisableFilter();
            ShowListData(this.list);
        }

        private void DisableFilter()
        {
            this.cbPrograma.Enabled = false;
            this.txtNroItemFilter.Enabled = false;
            this.button1.Enabled = false;
        }

        private void rbFiltroPrograma_CheckedChanged(object sender, EventArgs e)
        {
            this.cbPrograma.Enabled = true;
            this.txtNroItemFilter.Enabled = false;
            this.button1.Enabled = false;
        }

        private void cbPrograma_SelectedIndexChanged(object sender, EventArgs e)
        {
            var filterList = this.list.Where(pu => pu.DES_PROGRAMA.ToString().Contains(cbPrograma.Text.Trim()));
            ShowListData(filterList.ToList<PerfilUsuario>());
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if ((new TestDBHelper()).TestConnection())
            {
                if (this.lvItems.SelectedItems.Count > 0)
                {
                    var frm = new FrmUsuarioPerfil((PerfilUsuario)(this.lvItems.SelectedItems[0]).Tag);
                    frm.ShowDialog();
                    ShowListData(this.list);
                }
                else
                {
                    MessageBox.Show(this, "Debe seleccionar un item para editar");
                }
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Este proceso eliminará toda la configuración personalizada para el perfil de este usuario, desea continuar?", "Confirmacion de cambio de perfil", MessageBoxButtons.YesNo);
            if(result == DialogResult.Yes)
            {
                var frm = new FrmCambioPerfilUsuario(this.usuario);
                frm.ShowDialog();
                RefreshData();
                ShowListData(this.list);
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            var countp = this.updao.GetCountPermisos(this.usuario.COD_PERFIL);
            var countpu = this.updao.GetCountPermisosUsuario(this.usuario.C15_USUARIO);

            DialogResult result = MessageBox.Show("Este proceso Actualizara el perfil con los permisos faltantes. "
                                + " Faltan actualizar "+ (countp - countpu)+", desea continuar?", "Confirmacion de actualizacion perfil", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                if (bw.IsBusy != true)
                {
                    bw.RunWorkerAsync();
                }
                // pongo el mensaje de procesamiento
                this.frmLoading.StartPosition = FormStartPosition.CenterParent;

                this.frmLoading.ShowDialog(this);                
            }
        }


        
    }
}
