﻿using fa_security_ui.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fa_security_ui.Forms
{
    public partial class FrmSelectEmpPadre : Form
    {
        private List<EmpPadre> list;
        public EmpPadre value {get; set;}


        public FrmSelectEmpPadre(List<EmpPadre> list, EmpPadre value)
        {
            InitializeComponent();
            this.list = list;
            this.value = value;
            this.cbEmpresa.Items.AddRange(this.list.ToArray());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.cbEmpresa.SelectedItem != null)
            {
                this.value = (EmpPadre) this.cbEmpresa.SelectedItem;
                this.Close();
            }
            else
            {
                MessageBox.Show(this, "Debe seleccionar una empresa");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.value = null;
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void cbEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
