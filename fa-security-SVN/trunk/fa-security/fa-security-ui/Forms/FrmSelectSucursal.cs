﻿using fa_security_ui.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fa_security_ui.Forms
{
    public partial class FrmSelectSucursal : Form
    {
        private List<EmpSuc> list;
        public EmpSuc value {get; set;}
        public FrmSelectSucursal(List<EmpSuc> list, EmpSuc value)
        {
            InitializeComponent();
            this.list = list;
            this.value = value;
            this.cbEmpresa.Items.AddRange(this.list.ToArray());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.cbEmpresa.SelectedItem != null)
            {
                this.value = (EmpSuc)this.cbEmpresa.SelectedItem;
                this.Close();
            }
            else
            {
                MessageBox.Show(this, "Debe seleccionar una sucursal");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.value = null;
            this.Close();
        }
    }
}
