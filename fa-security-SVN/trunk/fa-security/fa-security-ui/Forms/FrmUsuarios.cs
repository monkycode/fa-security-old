﻿using fa_security_ui.Dao;
using fa_security_ui.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TezaUtils.Dao;
using TezaUtils.Forms;
using TezaUtils.Helper;
using TezaUtils.Model;

namespace fa_security_ui.Forms
{
    public partial class FrmUsuarios : Form
    {
        private UsuarioDAO udao;

        public FrmUsuarios()
        {
            this.udao = new UsuarioDAO();
            InitializeComponent();
            RefreshData();
        }

        private void RefreshData()
        {
            InitializeListView();

            var remitos = udao.GetUsuarios();

            lvUsuarios.BeginUpdate();
            lvUsuarios.Items.Clear();

            var items = new ListViewItem[remitos.Count];
            var itemNro = 0;

            foreach (var trx in remitos)
            {
                var item = CreateItemTrxlv(trx);

                items[itemNro++] = item;
            }
            lvUsuarios.Items.AddRange(items);
            lvUsuarios.EndUpdate();
            toolStripStatusLabel2.Text = remitos.Count().ToString();
            this.statusStrip1.Refresh();
        }

        private static ListViewItem CreateItemTrxlv(Usuario t)
        {
            string[] row = { t.C15_USUARIO, t.DES_USUARIO, t.COD_PERFIL};
            ListViewItem item = new ListViewItem(row);

            item.Tag = t;
            return item;
        }

        private void InitializeListView()
        {
            lvUsuarios.Columns.Clear();
            lvUsuarios.Columns.Add("Usuario");
            lvUsuarios.Columns.Add("Nombre");
            lvUsuarios.Columns.Add("Perfil");
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if ((new TestDBHelper()).TestConnection())
            {
                var frm = new FrmUsuario(null, TipoForm.ALTA);
                frm.ShowDialog();
                RefreshData();
            }
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if ((new TestDBHelper()).TestConnection())
            {
                if (this.lvUsuarios.SelectedItems.Count > 0)
                {
                    var frm = new FrmUsuarioPermisos((Usuario)(this.lvUsuarios.SelectedItems[0]).Tag);
                    frm.ShowDialog();
                    RefreshData();
                }
                else
                {
                    MessageBox.Show(this, "Debe seleccionar un usuario");
                }
            }
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            if ((new TestDBHelper()).TestConnection())
            {
                if (this.lvUsuarios.SelectedItems.Count > 0)
                {
                    var emppadao = new EmpPadreDao();
                    var frm = new FrmUsuarioEmpPadre((Usuario)(this.lvUsuarios.SelectedItems[0]).Tag);
                    frm.ShowDialog();
                    RefreshData();
                }
                else
                {
                    MessageBox.Show(this, "Debe seleccionar un usuario");
                }
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if ((new TestDBHelper()).TestConnection())
            {

                if (this.lvUsuarios.SelectedItems.Count > 0)
                {
                    var frm = new FrmUsuario((Usuario)this.lvUsuarios.SelectedItems[0].Tag, TipoForm.MODIFICACION);
                    frm.ShowDialog();
                    RefreshData();
                }
                else
                {
                    MessageBox.Show(this, "Debe seleccionar un usuario");
                }
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if ((new TestDBHelper()).TestConnection())
            {

                if (this.lvUsuarios.SelectedItems.Count > 0)
                {
                    var frm = new FrmUsuario((Usuario)this.lvUsuarios.SelectedItems[0].Tag, TipoForm.BAJA);
                    frm.ShowDialog();
                    RefreshData();
                }
                else
                {
                    MessageBox.Show(this, "Debe seleccionar un usuario");
                }
            }
        }
    }
}
