﻿using fa_security_ui.Dao;
using fa_security_ui.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TezaUtils.Helper;
using TezaUtils.Model;

namespace fa_security_ui.Forms
{
    public partial class FrmUsuarioEmpPadre : Form
    {

        private static readonly ILog Logger = LogManager.GetLogger(typeof(FrmUsuarioEmpPadre));



        private EmpPadreDao epdao;
        private Usuario uce;
        private List<EmpPadre> ephab;

        public FrmUsuarioEmpPadre(Usuario uce)
        {
            InitializeComponent();
            this.uce = uce;
            this.epdao = new EmpPadreDao();

            RefreshData();
        }

        private void RefreshData()
        {
            InitializeListView();
            this.textBox1.Text = this.uce.C15_USUARIO;
            this.textBox2.Text = this.uce.DES_USUARIO;
            this.ephab = epdao.GetEmpresasAsignadaUsuario(this.uce);
            lvEmpPadre.BeginUpdate();
            lvEmpPadre.Items.Clear();

            var items = new ListViewItem[this.ephab.Count];
            var itemNro = 0;

            foreach (var ep in this.ephab)
            {
                var item = CreateItemTrxlv(ep);

                items[itemNro++] = item;
            }
            lvEmpPadre.Items.AddRange(items);
            lvEmpPadre.EndUpdate();
            toolStripStatusLabel2.Text = this.ephab.Count().ToString();
            this.statusStrip1.Refresh();
        }

        private ListViewItem CreateItemTrxlv(EmpPadre ep)
        {
            string[] row = { ep.COD_EMP_PADRE, ep.ROB_RAZON, ep.ROB_FANTASIA };
            ListViewItem item = new ListViewItem(row);

            item.Tag = ep;
            return item;
        }

        private void InitializeListView()
        {
            lvEmpPadre.Columns.Clear();
            lvEmpPadre.Columns.Add("COD_EMP_PADRE");
            lvEmpPadre.Columns.Add("ROB_RAZON");
            lvEmpPadre.Columns.Add("ROB_FANTASIA");
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if ((new TestDBHelper()).TestConnection())
            {
                if (this.lvEmpPadre.SelectedItems.Count > 0)
                {
                    var usuconemp = new UsuarioConEmpresa();
                    usuconemp.emppadre = (EmpPadre)(this.lvEmpPadre.SelectedItems[0]).Tag;
                    usuconemp.usuario = uce;
                    var frm = new FrmUsuarioSucursal(usuconemp);
                    frm.ShowDialog();
                }
                else
                {
                    MessageBox.Show(this, "Debe seleccionar una empresa");
                }
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                var frm = new FrmSelectEmpPadre(this.epdao.GetEmpresasNOAsignadaUsuario(uce), null);
                frm.ShowDialog();
                if (frm.value != null)
                {
                    var ep = (EmpPadre)frm.value;
                    epdao.AgregarEmpPadreAUsuario(uce, ep);
                    MessageBox.Show("Empresa Asignada con Exito");
                    RefreshData();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                Logger.Error(ex.StackTrace);
                throw ex;
            }
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if ((new TestDBHelper()).TestConnection())
            {
                if (this.lvEmpPadre.SelectedItems.Count > 0)
                {
                    var em = (EmpPadre)(this.lvEmpPadre.SelectedItems[0]).Tag;
                    DialogResult result = MessageBox.Show("Desea eliminar la empresa? " + em.COD_EMP_PADRE +" | " + em.ROB_RAZON , "Confirmation", MessageBoxButtons.YesNoCancel);
                    if (result == DialogResult.Yes)
                    {
                        this.epdao.EliminarEmpPadreAUsuario(this.uce, em);
                    }
                    RefreshData();
                    MessageBox.Show(this, "Empresa eliminada con exito");
                }
                else
                {
                    MessageBox.Show(this, "Debe seleccionar una empresa");
                }
            }
        }
    }
}
