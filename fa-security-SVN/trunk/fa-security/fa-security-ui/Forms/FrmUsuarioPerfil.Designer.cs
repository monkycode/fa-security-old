﻿namespace fa_security_ui.Forms
{
    partial class FrmUsuarioPerfil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbAlta = new System.Windows.Forms.CheckBox();
            this.cbMenu = new System.Windows.Forms.CheckBox();
            this.cbBaja = new System.Windows.Forms.CheckBox();
            this.cbModifica = new System.Windows.Forms.CheckBox();
            this.cbAux1 = new System.Windows.Forms.CheckBox();
            this.cbAux2 = new System.Windows.Forms.CheckBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cbAlta
            // 
            this.cbAlta.AutoSize = true;
            this.cbAlta.Location = new System.Drawing.Point(60, 136);
            this.cbAlta.Name = "cbAlta";
            this.cbAlta.Size = new System.Drawing.Size(44, 17);
            this.cbAlta.TabIndex = 0;
            this.cbAlta.Text = "Alta";
            this.cbAlta.UseVisualStyleBackColor = true;
            // 
            // cbMenu
            // 
            this.cbMenu.AutoSize = true;
            this.cbMenu.Location = new System.Drawing.Point(60, 160);
            this.cbMenu.Name = "cbMenu";
            this.cbMenu.Size = new System.Drawing.Size(53, 17);
            this.cbMenu.TabIndex = 1;
            this.cbMenu.Text = "Menu";
            this.cbMenu.UseVisualStyleBackColor = true;
            // 
            // cbBaja
            // 
            this.cbBaja.AutoSize = true;
            this.cbBaja.Location = new System.Drawing.Point(60, 184);
            this.cbBaja.Name = "cbBaja";
            this.cbBaja.Size = new System.Drawing.Size(47, 17);
            this.cbBaja.TabIndex = 2;
            this.cbBaja.Text = "Baja";
            this.cbBaja.UseVisualStyleBackColor = true;
            // 
            // cbModifica
            // 
            this.cbModifica.AutoSize = true;
            this.cbModifica.Location = new System.Drawing.Point(163, 136);
            this.cbModifica.Name = "cbModifica";
            this.cbModifica.Size = new System.Drawing.Size(66, 17);
            this.cbModifica.TabIndex = 3;
            this.cbModifica.Text = "Modifica";
            this.cbModifica.UseVisualStyleBackColor = true;
            // 
            // cbAux1
            // 
            this.cbAux1.AutoSize = true;
            this.cbAux1.Location = new System.Drawing.Point(163, 160);
            this.cbAux1.Name = "cbAux1";
            this.cbAux1.Size = new System.Drawing.Size(50, 17);
            this.cbAux1.TabIndex = 4;
            this.cbAux1.Text = "Aux1";
            this.cbAux1.UseVisualStyleBackColor = true;
            // 
            // cbAux2
            // 
            this.cbAux2.AutoSize = true;
            this.cbAux2.Location = new System.Drawing.Point(163, 184);
            this.cbAux2.Name = "cbAux2";
            this.cbAux2.Size = new System.Drawing.Size(50, 17);
            this.cbAux2.TabIndex = 5;
            this.cbAux2.Text = "Aux2";
            this.cbAux2.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox2.Location = new System.Drawing.Point(73, 48);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(211, 20);
            this.textBox2.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Nro Item";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Info;
            this.textBox1.Location = new System.Drawing.Point(73, 22);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(211, 20);
            this.textBox1.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Usuario";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.Info;
            this.textBox3.Location = new System.Drawing.Point(73, 74);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(211, 20);
            this.textBox3.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Programa";
            // 
            // button2
            // 
            this.button2.Image = global::fa_security_ui.Properties.Resources.cancel;
            this.button2.Location = new System.Drawing.Point(195, 216);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 31);
            this.button2.TabIndex = 23;
            this.button2.Text = "Cancelar";
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Image = global::fa_security_ui.Properties.Resources.ok;
            this.button1.Location = new System.Drawing.Point(24, 216);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 31);
            this.button1.TabIndex = 22;
            this.button1.Text = "Aceptar";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.SystemColors.Info;
            this.textBox4.Location = new System.Drawing.Point(73, 100);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(211, 20);
            this.textBox4.TabIndex = 25;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Descripcion";
            // 
            // FrmUsuarioPerfil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 262);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbAux2);
            this.Controls.Add(this.cbAux1);
            this.Controls.Add(this.cbModifica);
            this.Controls.Add(this.cbBaja);
            this.Controls.Add(this.cbMenu);
            this.Controls.Add(this.cbAlta);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmUsuarioPerfil";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Usuario Perfil";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cbAlta;
        private System.Windows.Forms.CheckBox cbMenu;
        private System.Windows.Forms.CheckBox cbBaja;
        private System.Windows.Forms.CheckBox cbModifica;
        private System.Windows.Forms.CheckBox cbAux1;
        private System.Windows.Forms.CheckBox cbAux2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label4;
    }
}