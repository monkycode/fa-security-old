﻿using fa_security_ui.Dao;
using fa_security_ui.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TezaUtils.Forms;

namespace fa_security_ui.Forms
{
    public partial class FrmPerfil : Form
    {
        private Perfiles p;
        private TipoForm tipoForm;

        private PerfilesDao pdao;
        private PerfilABMDao abmPerfil;

        private BackgroundWorker bw;
        private FrmLoading frmLoading = new FrmLoading();
        
        public FrmPerfil(Perfiles p, TipoForm tipoForm)
        {
            InitializeComponent();
           
            this.p = p;
            this.tipoForm = tipoForm;

            this.pdao = new PerfilesDao();
            this.abmPerfil = new PerfilABMDao();

            this.bw = new BackgroundWorker();

            SetupForm(tipoForm);

            
            
            
        }

        private void SetupForm(TipoForm tipoForm) 
        {
            // Configuracion de los eventos de bw, en este caso configuro el completed porque el dowork depende de la solapa
            bw.RunWorkerCompleted += BwRunWorkerCompleted;
            // asgino metodo para informar cambios de estado
            bw.ProgressChanged += BwProgressChanged;

            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = true;

            if (tipoForm.Equals(TipoForm.ALTA))
            {
                RefreshData();
                this.p = new Perfiles();
                
                // asigno el delegate correspondiente
                bw.DoWork += BwDoWorkAltaPerfil;
            }
            if (tipoForm.Equals(TipoForm.MODIFICACION))
            {
                // asigno el delegate correspondiente
                bw.DoWork += BwDoWorkModificarPerfil;
                GetDataToControls(this.p);
                DisableControlModificacion();
            }
            if (tipoForm.Equals(TipoForm.BAJA))
            {
                // asigno el delegate correspondiente
                bw.DoWork += BwDoWorkBajaPerfil;
                GetDataToControls(this.p);
                DisableControlBaja();
            }
        }

        private void DisableControlBaja()
        {
            DisableControlModificacion();

            cbPrograma.Enabled = false;
            cbPrograma.BackColor = System.Drawing.SystemColors.Info;
            chkNvoPrograma.Enabled = false;
            txtNvoPrograma.ReadOnly = true;
            txtNvoPrograma.BackColor = System.Drawing.SystemColors.Info;

            cbxMenu.Enabled = false;
            cbxMenu.BackColor = System.Drawing.SystemColors.Info;
            chkNvoMenu.Enabled = false;
            txtNvoMenu.ReadOnly = true;
            txtNvoMenu.BackColor = System.Drawing.SystemColors.Info;

            cbAlta.Enabled = false;
            cbMenu.Enabled = false;
            cbBaja.Enabled = false;
            cbModifica.Enabled = false;
            cbAux1.Enabled = false;
            cbAux2.Enabled = false;
            
        }

        private void DisableControlModificacion()
        {
            cbPerfil.Enabled = false;
            cbPerfil.BackColor = System.Drawing.SystemColors.Info;
            chkNvoPerfil.Enabled = false;
            txtNvoPerfil.Enabled = false;
            txtNvoPerfil.BackColor = System.Drawing.SystemColors.Info;

            txtNroItem.Enabled = false;
            txtNroItem.BackColor = System.Drawing.SystemColors.Info;

            cbxMenu.Items.AddRange(pdao.GetMenu().ToArray());
            


        }

        private void BwRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.frmLoading.Hide();
            MessageBox.Show("Operacion realizada con Exito");
            this.Close();
        }

        private void BwDoWorkBajaPerfil(object sender, DoWorkEventArgs e)
        {
            this.abmPerfil.DeletePerfil(this.p);
        }

        private void BwDoWorkModificarPerfil(object sender, DoWorkEventArgs e)
        {
            this.abmPerfil.UpdatePerfil(this.p);
        }

        private void BwDoWorkAltaPerfil(object sender, DoWorkEventArgs e)
        {
            this.abmPerfil.AddPerfil(this.p);
        }

        private void BwProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            throw new NotImplementedException();
        }
        private void RefreshData()
        {
            txtNvoPerfil.Enabled = false;
            txtNvoPrograma.Enabled = false;
            txtNvoMenu.Enabled = false;
            cbPrograma.Items.Clear();
            cbPrograma.Items.AddRange(pdao.GetProgramas().ToArray());
            cbPerfil.Items.Clear();
            cbPerfil.Items.AddRange(pdao.GetPerfil().ToArray());
            cbxMenu.Items.Clear();
            cbxMenu.Items.AddRange(pdao.GetMenu().ToArray());
            //this.p = new Perfiles();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //TODO Falta validar el ingreso de datos!!!!

            if (this.tipoForm.Equals(TipoForm.ALTA) || this.tipoForm.Equals(TipoForm.MODIFICACION))
            {
                    GetDataFromControls(this.p);
                    if (bw.IsBusy != true)
                    {
                        bw.RunWorkerAsync();
                    }
                    // pongo el mensaje de procesamiento
                    this.frmLoading.StartPosition = FormStartPosition.CenterParent;
                    this.frmLoading.ShowDialog(this);
                
            }
            else
            {
                DialogResult result = MessageBox.Show("Esta seguro que desea eliminar el usuario?", "Confirmacion de Eliminacion de usuario", MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    if (bw.IsBusy != true)
                    {
                        bw.RunWorkerAsync();
                    }
                    // pongo el mensaje de procesamiento
                    this.frmLoading.StartPosition = FormStartPosition.CenterParent;

                    this.frmLoading.ShowDialog(this);
                }
            }
            
        }

        private void GetDataFromControls(Perfiles p)
        {
            if (this.cbPerfil.Text == "")
            {
                p.COD_PERFIL = this.txtNvoPerfil.Text;
            }
            else { p.COD_PERFIL = this.cbPerfil.Text; }
            
            p.NRO_ITEM = Convert.ToDecimal (this.txtNroItem.Text);
            
            if (this.cbPrograma.Text == "")
            {
                p.DES_PROGRAMA = this.txtNvoPrograma.Text;
            }
            else { p.DES_PROGRAMA = this.cbPrograma.Text; }

            if (this.cbxMenu.Text == "")
            { 
                p.DES_MENU = this.txtNvoMenu.Text; 
            }
            else { p.DES_MENU = this.cbxMenu.Text; }

            p.EST_ALTA = (this.cbAlta.Checked)? "T":"F";
            p.EST_AUX1 = (this.cbAux1.Checked)? "T":"F";
            p.EST_AUX2= (this.cbAux2.Checked)? "T":"F";
            p.EST_BAJA = (this.cbBaja.Checked)? "T":"F";
            p.EST_MENU = (this.cbMenu.Checked)? "T":"F";
            p.EST_MODIFICA = (this.cbModifica.Checked)? "T" : "F";
 
        }

        private void GetDataToControls(Perfiles p)
        {
            cbPerfil.Text = p.COD_PERFIL;
            txtNroItem.Text =Convert.ToString(p.NRO_ITEM);
            cbPrograma.Text = p.DES_PROGRAMA;
            cbxMenu.Text = p.DES_MENU;
            cbAlta.Checked = (p.EST_ALTA.Equals("T")) ? true : false;
            cbAux1.Checked = (p.EST_AUX1.Equals("T")) ? true : false;
            cbAux2.Checked = (p.EST_AUX2.Equals("T")) ? true : false;
            cbBaja.Checked = (p.EST_MENU.Equals("T")) ? true : false;
            cbMenu.Checked = (p.EST_ALTA.Equals("T")) ? true : false;
            cbModifica.Checked = (p.EST_MODIFICA.Equals("T")) ? true : false;

        }
        private void chkNvoPerfil_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chkNvoPerfil.Checked == true)
            { this.cbPerfil.Enabled = false; this.txtNvoPerfil.Enabled = true; }
            else { this.cbPerfil.Enabled = true; this.txtNvoPerfil.Enabled = false; }
            
        }

        private void chkNvoPrograma_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chkNvoPrograma.Checked == true)
            { this.cbPrograma.Enabled = false; this.txtNvoPrograma.Enabled = true; }
            else { this.cbPrograma.Enabled = true; this.txtNvoPrograma.Enabled = false; }

        }

        private void chkNvoMenu_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chkNvoMenu.Checked == true)
            { this.cbxMenu.Enabled = false;txtNvoMenu.Enabled=true; }
            else { this.cbxMenu.Enabled = true; txtNvoMenu.Enabled = false; }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
