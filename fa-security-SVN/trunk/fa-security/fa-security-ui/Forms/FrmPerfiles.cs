﻿using fa_security_ui.Dao;
using fa_security_ui.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TezaUtils.Dao;
using TezaUtils.Forms;
using TezaUtils.Helper;

namespace fa_security_ui.Forms
{
    public partial class FrmPerfiles : Form
    {
        private PerfilesDao pdao;
        public List<Perfiles> list { get; set; }
        public FrmPerfiles()
        {
            InitializeComponent();
            this.pdao = new PerfilesDao();
            DisableFilter();
            // busca la data de la bd y la guarda en la lista
            RefreshData();
            // el rb sin filtro muestra la lista en la grilla
            rbSinFiltro.Checked = true;
            InitializeListView();
        }

        private void DisableFilter()
        {
            this.cbPrograma.Enabled = false;
            this.txtNroItemFilter.Enabled = false;
            this.button1.Enabled = false;
        }

        private void RefreshData()
        {
            InitializeListView();
            this.list = pdao.GetPerfiles();
            this.cbPrograma.Items.Clear();
            this.cbPrograma.Items.AddRange(pdao.GetProgramas().ToArray());
            ShowListData(this.list);
                        
        }

        private void ShowListData(List<Perfiles> list)
        {
            lvItems.BeginUpdate();
            lvItems.Items.Clear();

            var items = new ListViewItem[list.Count];
            var itemNro = 0;

            foreach (var es in list)
            {
                var item = CreateItemTrxlv(es);

                items[itemNro++] = item;
            }
            lvItems.Items.AddRange(items);
            lvItems.EndUpdate();
            toolStripStatusLabel2.Text = list.Count().ToString();
            this.statusStrip1.Refresh();
        }
        private void InitializeListView()
        {
            lvItems.Columns.Clear();
            lvItems.Columns.Add("COD_PERFIL");
            lvItems.Columns.Add("NRO_ITEM");
            lvItems.Columns.Add("DES_PROGRAMA");
            lvItems.Columns.Add("DES_MENU");
            lvItems.Columns.Add("EST_MENU");
            lvItems.Columns.Add("EST_ALTA");
            lvItems.Columns.Add("EST_BAJA");
            lvItems.Columns.Add("EST_MODIFICA");
            lvItems.Columns.Add("EST_AUX1");
            lvItems.Columns.Add("EST_AUX2");

        }

        private static ListViewItem CreateItemTrxlv(Perfiles p)
        {
            string[] row = { p.COD_PERFIL, 
                             Convert.ToString(p.NRO_ITEM), 
                             p.DES_PROGRAMA,
                             p.DES_MENU,
                             p.EST_MENU,
                             p.EST_ALTA,
                             p.EST_BAJA,
                             p.EST_MODIFICA,
                             p.EST_AUX1,
                             p.EST_AUX2 
                           };
            ListViewItem item = new ListViewItem(row);
            
            item.Tag = p;
            return item;
        }

        private void rbFiltroItem_CheckedChanged(object sender, EventArgs e)
        {
            this.cbPrograma.Enabled = false;
            this.txtNroItemFilter.Enabled = true;
            this.button1.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var filterList = this.list.Where(p => p.NRO_ITEM.ToString().Contains(txtNroItemFilter.Text.Trim()));
            ShowListData(filterList.ToList<Perfiles>());
        }

        private void rbSinFiltro_CheckedChanged(object sender, EventArgs e)
        {
            DisableFilter();
            ShowListData(this.list);
        }

        private void rbFiltroPrograma_CheckedChanged(object sender, EventArgs e)
        {
            this.cbPrograma.Enabled = true;
            this.txtNroItemFilter.Enabled = false;
            this.button1.Enabled = false;
        }

        private void cbPrograma_SelectedIndexChanged(object sender, EventArgs e)
        {
            var filterList = this.list.Where(p => p.DES_PROGRAMA.ToString().Contains(cbPrograma.Text.Trim()));
            ShowListData(filterList.ToList<Perfiles>());
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if ((new TestDBHelper()).TestConnection())
            {
                var frm = new FrmPerfil(null, TipoForm.ALTA);
                frm.ShowDialog();
                RefreshData();
                
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if ((new TestDBHelper()).TestConnection())
            {

                if (this.lvItems.SelectedItems.Count > 0)
                {
                    var frm = new FrmPerfil((Perfiles)this.lvItems.SelectedItems[0].Tag, TipoForm.MODIFICACION);
                    frm.ShowDialog();
                    RefreshData();
                }
                else
                {
                    MessageBox.Show(this, "Debe seleccionar un Perfil");
                }
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {

            if ((new TestDBHelper()).TestConnection())
            {

                if (this.lvItems.SelectedItems.Count > 0)
                {
                    var frm = new FrmPerfil((Perfiles)this.lvItems.SelectedItems[0].Tag, TipoForm.BAJA);
                    frm.ShowDialog();
                    RefreshData();
                }
                else
                {
                    MessageBox.Show(this, "Debe seleccionar un Perfil");
                }
            }
        }
    }
}
