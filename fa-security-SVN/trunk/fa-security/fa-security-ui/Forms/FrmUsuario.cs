﻿using fa_security_ui.Dao;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TezaUtils.Forms;
using TezaUtils.Helper;
using TezaUtils.Model;

namespace fa_security_ui.Forms
{
    public partial class FrmUsuario : Form
    {
        private Usuario usuario;
        private TipoForm tipoform;

        private UsuarioPerfilDao updao;
        private UsuarioABMDao uabmdao;
        private TezaCryptoHelper tch;

        private BackgroundWorker bw;
        private FrmLoading frmLoading = new FrmLoading();


        public FrmUsuario(Usuario usuario, TipoForm tipoform)
        {
            InitializeComponent(); 
            
            this.usuario = usuario;
            this.tipoform = tipoform;
            
            this.updao = new UsuarioPerfilDao();
            this.uabmdao = new UsuarioABMDao();

            this.bw = new BackgroundWorker();

            this.tch = new TezaCryptoHelper();

            this.cbPerfil.Items.AddRange(updao.GetPerfiles().ToArray());

            SetupForm(tipoform);
        }

        private void SetupForm(TipoForm tipoform)
        {
            // Configuracion de los eventos de bw, en este caso configuro el completed porque el dowork depende de la solapa
            bw.RunWorkerCompleted += BwRunWorkerCompleted;
            // asgino metodo para informar cambios de estado
            bw.ProgressChanged += BwProgressChanged;

            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = true;


            if (tipoform.Equals(TipoForm.ALTA))
            {
                this.usuario = new Usuario();
                dtpFechaAlta.Value = DateTime.Now;
                cbHabilitado.Checked = true;
                // asigno el delegate correspondiente
                bw.DoWork += BwDoWorkAltaUsuario;
            }
            if (tipoform.Equals(TipoForm.MODIFICACION))
            {
                // asigno el delegate correspondiente
                bw.DoWork += BwDoWorkMeodificarUsuario;
                GetDataToControls(this.usuario);
                DisableControlModificacion();
            }
            if (tipoform.Equals(TipoForm.BAJA))
            {
                // asigno el delegate correspondiente
                bw.DoWork += BwDoWorkBajaUsuario;
                GetDataToControls(this.usuario);
                DisableControlBaja();
            }

        }

        private void DisableControlBaja()
        {
            DisableControlModificacion();

            txtNombre.ReadOnly = true;
            txtNombre.BackColor = System.Drawing.SystemColors.Info;

            cbHabilitado.Enabled = false;

            cbAutenticacionSMTP.Enabled = false;
            cbClaveEncriptada.Enabled = false;
            cbConexionSeguraSMTP.Enabled = false;

            txtPuertoSMTP.ReadOnly = true;
            txtPuertoSMTP.BackColor = System.Drawing.SystemColors.Info;

            txtServidorSMTP.ReadOnly = true;
            txtServidorSMTP.BackColor = System.Drawing.SystemColors.Info;

            txtUsuarioSMTP.ReadOnly = true;
            txtUsuarioSMTP.BackColor = System.Drawing.SystemColors.Info;

            txtCorreoSMTP.ReadOnly = true;
            txtCorreoSMTP.BackColor = System.Drawing.SystemColors.Info;

            txtClaveSMTP.ReadOnly = true;
            txtClaveSMTP.BackColor = System.Drawing.SystemColors.Info;
        }

        private void DisableControlModificacion()
        {
            txtUsuario.ReadOnly = true;
            txtUsuario.BackColor = System.Drawing.SystemColors.Info;
            txtUsuario.Enabled = false;

            txtClave1.Enabled = false;
            txtClave2.Enabled = false;

            cbPerfil.Enabled = false;
            cbPerfil.BackColor = System.Drawing.SystemColors.Info;

            dtpFechaAlta.Enabled = false;
            dtpFechaAlta.BackColor = System.Drawing.SystemColors.Info;
        }

        private void BwRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.frmLoading.Hide();
            MessageBox.Show("Operacion realizada con Exito");
            this.Close();
        }

        private void BwDoWorkBajaUsuario(object sender, DoWorkEventArgs e)
        {
            this.uabmdao.DeleteUsuario(this.usuario); 
        }

        private void BwDoWorkMeodificarUsuario(object sender, DoWorkEventArgs e)
        {
            this.uabmdao.UpdateUsuario(this.usuario);
        }

        private void BwDoWorkAltaUsuario(object sender, DoWorkEventArgs e)
        {
            this.uabmdao.AddUsuario(this.usuario);
        }

        private void BwProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            throw new NotImplementedException();
        }
               
        private Boolean ValidateForm()
        {
            var result = false;
            if (ValidarUsuario() && ValidarNombre() && ValidarClave() && ValidarPerfil())
            {
                result = true;
            }
            return result;
        }

        private Boolean ValidarClave()
        {
            var result = false;
            if (tipoform.Equals(TipoForm.ALTA))
            {

                if (txtClave1.Text == "")
                {
                    errorProvider1.SetError(txtClave1, "El clave no puede ser vacia");
                }
                else
                {
                    if (txtClave2.Text == "")
                    {
                        errorProvider1.SetError(txtClave2, "El clave no puede ser vacia");
                    }
                    else
                    {
                        if (!(txtClave2.Text.Equals(txtClave1.Text)))
                        {
                            errorProvider1.SetError(txtClave2, "Las claves deben ser iguales");
                            errorProvider1.SetError(txtClave1, "Las claves deben ser iguales");
                        }
                        else
                        {
                            result = true;
                            errorProvider1.SetError(txtClave1, "");
                            errorProvider1.SetError(txtClave2, "");
                        }
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        private Boolean ValidarNombre()
        {
            var result = false;
            if (txtNombre.Text == "")
            {
                errorProvider1.SetError(txtNombre, "El nombre no puede ser vacio");
            }
            else
            {
                result = true;
                errorProvider1.SetError(txtNombre, "");
            }
            return result;
        }

        private Boolean ValidarUsuario()
        {
            var result = false;

            if (tipoform.Equals(TipoForm.ALTA))
            {
                if (txtUsuario.Text == "")
                {
                    errorProvider1.SetError(txtUsuario, "El usuario no puede ser vacio");
                }
                else
                {
                    if (!(uabmdao.UsuarioDisponible(txtUsuario.Text)))
                    {
                        errorProvider1.SetError(txtUsuario, "El usuario: " + txtUsuario.Text + "ya esta en uso y no se puede repetir, debe modificar el usuario");
                    }
                    else
                    {
                        result = true;
                        errorProvider1.SetError(txtUsuario, "");
                    }
                }          
            }
            else
            {
                result = true;
            }
            return result;
        }

        private Boolean ValidarPerfil()
        {
            var result = false;
            if (cbPerfil.Text == "")
            {
                errorProvider1.SetError(cbPerfil, "Debe seleccionar un perfil.");
            }
            else
            {

                result = true;
                errorProvider1.SetError(cbPerfil, "");
            }
            return result;
        }

        private void txtUsuario_Leave(object sender, EventArgs e)
        {
            ValidarUsuario();
        }

        private void txtNombre_Leave(object sender, EventArgs e)
        {
            ValidarNombre();
        }

        private void txtClave1_Leave(object sender, EventArgs e)
        {
            ValidarClave();
        }

        private void txtClave2_Leave(object sender, EventArgs e)
        {
            ValidarClave();
        }

        private void cbPerfil_Leave(object sender, EventArgs e)
        {
            ValidarPerfil();
        }
        
        private void bnAceptar_Click(object sender, EventArgs e)
        {
            if (this.tipoform.Equals(TipoForm.ALTA) || this.tipoform.Equals(TipoForm.MODIFICACION))
            {
                if (ValidateForm())
                {
                    GetDataFromControls(this.usuario);
                    if (bw.IsBusy != true)
                    {
                        bw.RunWorkerAsync();
                    }
                    // pongo el mensaje de procesamiento
                    this.frmLoading.StartPosition = FormStartPosition.CenterParent;
                    this.frmLoading.ShowDialog(this);
                }
                else
                {
                    MessageBox.Show("Por favor revise los campos ingresados");
                }
            }
            else
            {
                DialogResult result = MessageBox.Show("Esta seguro que desea eliminar el usuario?", "Confirmacion de Eliminacion de usuario", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    if (bw.IsBusy != true)
                    {
                        bw.RunWorkerAsync();
                    }
                    // pongo el mensaje de procesamiento
                    this.frmLoading.StartPosition = FormStartPosition.CenterParent;

                    this.frmLoading.ShowDialog(this);
                }
            }
        }
        
        private void GetDataFromControls(Usuario u)
        {
            u.C15_USUARIO = txtUsuario.Text;
            u.COD_PERFIL = cbPerfil.Text;
            u.DES_USUARIO = txtNombre.Text;
            
            u.FEC_ALTA = dtpFechaAlta.Value;
            u.EST_HABILITADO = (cbHabilitado.Checked) ? "S":"N";

            u.EST_AUTORIZACION = (cbAutenticacionSMTP.Checked) ? "S" : "N";
            u.EST_ENCRIPTADO = (cbClaveEncriptada.Checked) ? "S" : "N"; 
            if (u.EST_ENCRIPTADO.Equals("S"))
            {
                u.RDE_PASSWORD = this.tch.Encode(txtClave1.Text);
            }
            else
            {
                u.RDE_PASSWORD = txtClave1.Text;
            }
            u.EST_SSL = (cbConexionSeguraSMTP.Checked) ? "S":"N";
            u.NRO_PUERTO_SMTP = (!(txtPuertoSMTP.Text.Equals(""))) ? Int32.Parse(txtPuertoSMTP.Text) : 0;
            u.RDE_HOST_SMTP = txtServidorSMTP.Text;
            u.RDE_LOGIN_SMTP = txtUsuarioSMTP.Text;
            u.RDE_MAIL = txtCorreoSMTP.Text;
            u.RDE_PASS_SMTP = txtClaveSMTP.Text;
        }

        private void GetDataToControls(Usuario u)
        {
            txtUsuario.Text = u.C15_USUARIO;
            cbPerfil.Text = u.COD_PERFIL;
            txtNombre.Text = u.DES_USUARIO;

            dtpFechaAlta.Value = u.FEC_ALTA;
            cbHabilitado.Checked = (u.EST_HABILITADO.Equals("S")) ? true : false;

            cbAutenticacionSMTP.Checked = (u.EST_AUTORIZACION.Equals("S")) ? true : false;
            cbClaveEncriptada.Checked = (u.EST_ENCRIPTADO.Equals("S")) ? true : false;

            cbConexionSeguraSMTP.Checked = (u.EST_SSL.Equals("S")) ? true : false;
            txtPuertoSMTP.Text = u.NRO_PUERTO_SMTP.ToString();
            txtServidorSMTP.Text = u.RDE_HOST_SMTP;
            txtUsuarioSMTP.Text = u.RDE_LOGIN_SMTP;
            txtCorreoSMTP.Text = u.RDE_MAIL;
            txtClaveSMTP.Text = u.RDE_PASS_SMTP;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
       
    }
}
