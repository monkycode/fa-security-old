﻿namespace fa_security_ui.Forms
{
    partial class FrmPerfil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkNvoMenu = new System.Windows.Forms.CheckBox();
            this.chkNvoPrograma = new System.Windows.Forms.CheckBox();
            this.chkNvoPerfil = new System.Windows.Forms.CheckBox();
            this.cbxMenu = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.cbAux2 = new System.Windows.Forms.CheckBox();
            this.cbAux1 = new System.Windows.Forms.CheckBox();
            this.cbModifica = new System.Windows.Forms.CheckBox();
            this.cbBaja = new System.Windows.Forms.CheckBox();
            this.cbMenu = new System.Windows.Forms.CheckBox();
            this.cbAlta = new System.Windows.Forms.CheckBox();
            this.cbPrograma = new System.Windows.Forms.ComboBox();
            this.txtNvoPrograma = new System.Windows.Forms.TextBox();
            this.txtNvoMenu = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNroItem = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNvoPerfil = new System.Windows.Forms.TextBox();
            this.cbPerfil = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chkNvoMenu);
            this.panel1.Controls.Add(this.chkNvoPrograma);
            this.panel1.Controls.Add(this.chkNvoPerfil);
            this.panel1.Controls.Add(this.cbxMenu);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.cbAux2);
            this.panel1.Controls.Add(this.cbAux1);
            this.panel1.Controls.Add(this.cbModifica);
            this.panel1.Controls.Add(this.cbBaja);
            this.panel1.Controls.Add(this.cbMenu);
            this.panel1.Controls.Add(this.cbAlta);
            this.panel1.Controls.Add(this.cbPrograma);
            this.panel1.Controls.Add(this.txtNvoPrograma);
            this.panel1.Controls.Add(this.txtNvoMenu);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtNroItem);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtNvoPerfil);
            this.panel1.Controls.Add(this.cbPerfil);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(348, 355);
            this.panel1.TabIndex = 0;
            // 
            // chkNvoMenu
            // 
            this.chkNvoMenu.AutoSize = true;
            this.chkNvoMenu.Location = new System.Drawing.Point(26, 185);
            this.chkNvoMenu.Name = "chkNvoMenu";
            this.chkNvoMenu.Size = new System.Drawing.Size(88, 17);
            this.chkNvoMenu.TabIndex = 30;
            this.chkNvoMenu.Text = "Nuevo Menú";
            this.chkNvoMenu.UseVisualStyleBackColor = true;
            this.chkNvoMenu.CheckedChanged += new System.EventHandler(this.chkNvoMenu_CheckedChanged);
            // 
            // chkNvoPrograma
            // 
            this.chkNvoPrograma.AutoSize = true;
            this.chkNvoPrograma.Location = new System.Drawing.Point(8, 132);
            this.chkNvoPrograma.Name = "chkNvoPrograma";
            this.chkNvoPrograma.Size = new System.Drawing.Size(106, 17);
            this.chkNvoPrograma.TabIndex = 29;
            this.chkNvoPrograma.Text = "Nuevo Programa";
            this.chkNvoPrograma.UseVisualStyleBackColor = true;
            this.chkNvoPrograma.CheckedChanged += new System.EventHandler(this.chkNvoPrograma_CheckedChanged);
            // 
            // chkNvoPerfil
            // 
            this.chkNvoPerfil.AutoSize = true;
            this.chkNvoPerfil.Location = new System.Drawing.Point(30, 49);
            this.chkNvoPerfil.Name = "chkNvoPerfil";
            this.chkNvoPerfil.Size = new System.Drawing.Size(84, 17);
            this.chkNvoPerfil.TabIndex = 28;
            this.chkNvoPerfil.Text = "Nuevo Perfil";
            this.chkNvoPerfil.UseVisualStyleBackColor = true;
            this.chkNvoPerfil.CheckedChanged += new System.EventHandler(this.chkNvoPerfil_CheckedChanged);
            // 
            // cbxMenu
            // 
            this.cbxMenu.BackColor = System.Drawing.SystemColors.Info;
            this.cbxMenu.FormattingEnabled = true;
            this.cbxMenu.Location = new System.Drawing.Point(120, 156);
            this.cbxMenu.Name = "cbxMenu";
            this.cbxMenu.Size = new System.Drawing.Size(211, 21);
            this.cbxMenu.TabIndex = 27;
            // 
            // button2
            // 
            this.button2.Image = global::fa_security_ui.Properties.Resources.cancel;
            this.button2.Location = new System.Drawing.Point(222, 302);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 31);
            this.button2.TabIndex = 25;
            this.button2.Text = "Cancelar";
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Image = global::fa_security_ui.Properties.Resources.ok;
            this.button1.Location = new System.Drawing.Point(51, 302);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 31);
            this.button1.TabIndex = 24;
            this.button1.Text = "Aceptar";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cbAux2
            // 
            this.cbAux2.AutoSize = true;
            this.cbAux2.Location = new System.Drawing.Point(195, 270);
            this.cbAux2.Name = "cbAux2";
            this.cbAux2.Size = new System.Drawing.Size(53, 17);
            this.cbAux2.TabIndex = 18;
            this.cbAux2.Text = "Aux 2";
            this.cbAux2.UseVisualStyleBackColor = true;
            // 
            // cbAux1
            // 
            this.cbAux1.AutoSize = true;
            this.cbAux1.Location = new System.Drawing.Point(195, 247);
            this.cbAux1.Name = "cbAux1";
            this.cbAux1.Size = new System.Drawing.Size(53, 17);
            this.cbAux1.TabIndex = 17;
            this.cbAux1.Text = "Aux 1";
            this.cbAux1.UseVisualStyleBackColor = true;
            // 
            // cbModifica
            // 
            this.cbModifica.AutoSize = true;
            this.cbModifica.Location = new System.Drawing.Point(195, 224);
            this.cbModifica.Name = "cbModifica";
            this.cbModifica.Size = new System.Drawing.Size(66, 17);
            this.cbModifica.TabIndex = 16;
            this.cbModifica.Text = "Modifica";
            this.cbModifica.UseVisualStyleBackColor = true;
            // 
            // cbBaja
            // 
            this.cbBaja.AutoSize = true;
            this.cbBaja.Location = new System.Drawing.Point(87, 270);
            this.cbBaja.Name = "cbBaja";
            this.cbBaja.Size = new System.Drawing.Size(47, 17);
            this.cbBaja.TabIndex = 15;
            this.cbBaja.Text = "Baja";
            this.cbBaja.UseVisualStyleBackColor = true;
            // 
            // cbMenu
            // 
            this.cbMenu.AutoSize = true;
            this.cbMenu.Location = new System.Drawing.Point(87, 247);
            this.cbMenu.Name = "cbMenu";
            this.cbMenu.Size = new System.Drawing.Size(53, 17);
            this.cbMenu.TabIndex = 14;
            this.cbMenu.Text = "Menú";
            this.cbMenu.UseVisualStyleBackColor = true;
            // 
            // cbAlta
            // 
            this.cbAlta.AutoSize = true;
            this.cbAlta.Location = new System.Drawing.Point(87, 224);
            this.cbAlta.Name = "cbAlta";
            this.cbAlta.Size = new System.Drawing.Size(44, 17);
            this.cbAlta.TabIndex = 13;
            this.cbAlta.Text = "Alta";
            this.cbAlta.UseVisualStyleBackColor = true;
            // 
            // cbPrograma
            // 
            this.cbPrograma.BackColor = System.Drawing.SystemColors.Info;
            this.cbPrograma.FormattingEnabled = true;
            this.cbPrograma.Location = new System.Drawing.Point(120, 103);
            this.cbPrograma.Name = "cbPrograma";
            this.cbPrograma.Size = new System.Drawing.Size(211, 21);
            this.cbPrograma.TabIndex = 11;
            // 
            // txtNvoPrograma
            // 
            this.txtNvoPrograma.BackColor = System.Drawing.SystemColors.Info;
            this.txtNvoPrograma.Location = new System.Drawing.Point(120, 130);
            this.txtNvoPrograma.Name = "txtNvoPrograma";
            this.txtNvoPrograma.Size = new System.Drawing.Size(211, 20);
            this.txtNvoPrograma.TabIndex = 10;
            // 
            // txtNvoMenu
            // 
            this.txtNvoMenu.BackColor = System.Drawing.SystemColors.Info;
            this.txtNvoMenu.Location = new System.Drawing.Point(120, 183);
            this.txtNvoMenu.Name = "txtNvoMenu";
            this.txtNvoMenu.Size = new System.Drawing.Size(211, 20);
            this.txtNvoMenu.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(80, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Menú";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(62, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Programa";
            // 
            // txtNroItem
            // 
            this.txtNroItem.BackColor = System.Drawing.SystemColors.Info;
            this.txtNroItem.Location = new System.Drawing.Point(120, 77);
            this.txtNroItem.Name = "txtNroItem";
            this.txtNroItem.Size = new System.Drawing.Size(211, 20);
            this.txtNroItem.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(67, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Nro Item";
            // 
            // txtNvoPerfil
            // 
            this.txtNvoPerfil.BackColor = System.Drawing.SystemColors.Info;
            this.txtNvoPerfil.Location = new System.Drawing.Point(120, 47);
            this.txtNvoPerfil.Name = "txtNvoPerfil";
            this.txtNvoPerfil.Size = new System.Drawing.Size(211, 20);
            this.txtNvoPerfil.TabIndex = 3;
            // 
            // cbPerfil
            // 
            this.cbPerfil.BackColor = System.Drawing.SystemColors.Info;
            this.cbPerfil.FormattingEnabled = true;
            this.cbPerfil.Location = new System.Drawing.Point(120, 20);
            this.cbPerfil.Name = "cbPerfil";
            this.cbPerfil.Size = new System.Drawing.Size(211, 21);
            this.cbPerfil.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(84, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Perfil";
            // 
            // FrmPerfil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 355);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmPerfil";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FrmPerfil";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cbPerfil;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNvoPerfil;
        private System.Windows.Forms.TextBox txtNroItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNvoMenu;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbPrograma;
        private System.Windows.Forms.TextBox txtNvoPrograma;
        private System.Windows.Forms.CheckBox cbAlta;
        private System.Windows.Forms.CheckBox cbAux2;
        private System.Windows.Forms.CheckBox cbAux1;
        private System.Windows.Forms.CheckBox cbModifica;
        private System.Windows.Forms.CheckBox cbBaja;
        private System.Windows.Forms.CheckBox cbMenu;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cbxMenu;
        private System.Windows.Forms.CheckBox chkNvoPerfil;
        private System.Windows.Forms.CheckBox chkNvoPrograma;
        private System.Windows.Forms.CheckBox chkNvoMenu;
    }
}