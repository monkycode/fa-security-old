﻿using fa_security_ui.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TezaUtils.Helper;

namespace fa_security_ui
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void usuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ((new TestDBHelper()).TestConnection())
            {
                var frm = new FrmUsuarios();
                frm.ShowDialog();
            }
        }

        private void perfilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ((new TestDBHelper()).TestConnection()) 
            {
                var frm = new FrmPerfiles();
                frm.ShowDialog();
            }
        }
    }
}
