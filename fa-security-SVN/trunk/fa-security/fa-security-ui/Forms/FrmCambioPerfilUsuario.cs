﻿using fa_security_ui.Dao;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TezaUtils.Dao;
using TezaUtils.Forms;
using TezaUtils.Model;

namespace fa_security_ui.Forms
{
    public partial class FrmCambioPerfilUsuario : Form
    {
        private Usuario usuario;
        private String nuevoPerfil;


        private UsuarioPerfilDao updao;

        private BackgroundWorker bw;

        private FrmLoading frmLoading = new FrmLoading();

        public FrmCambioPerfilUsuario(Usuario usuario)
        {
            InitializeComponent();


            this.bw = new BackgroundWorker();

            // Configuracion de los eventos de bw, en este caso configuro el completed porque el dowork depende de la solapa
            bw.RunWorkerCompleted += BwRunWorkerCompleted;
            // asigno el delegate correspondiente
            bw.DoWork += BwDoWorkCambiarPerfil;
            // asgino metodo para informar cambios de estado
            bw.ProgressChanged += BwProgressChanged;

            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = true;

            this.usuario = usuario;
            this.textBox1.Text = this.usuario.C15_USUARIO;
            this.textBox2.Text = this.usuario.DES_USUARIO;
            this.textBox3.Text = this.usuario.COD_PERFIL;

            this.updao = new UsuarioPerfilDao();

            this.comboBox1.Items.AddRange(updao.GetPerfiles().ToArray());
        }

        private void BwProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void BwDoWorkCambiarPerfil(object sender, DoWorkEventArgs e)
        {
            this.updao.CambiarPerfil(this.usuario, this.nuevoPerfil);

        }

        private void BwRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.frmLoading.Hide();
            MessageBox.Show("Perfil actualizado con Exito");
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Corro el bw con la logica de el procesamiento
            if (bw.IsBusy != true)
            {
                bw.RunWorkerAsync();
            }
            // pongo el mensaje de procesamiento
            this.frmLoading.StartPosition = FormStartPosition.CenterParent;

            this.frmLoading.ShowDialog(this);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.nuevoPerfil = this.comboBox1.SelectedItem.ToString();
        }
    }
}
