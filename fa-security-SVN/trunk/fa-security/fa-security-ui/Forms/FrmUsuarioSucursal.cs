﻿using fa_security_ui.Dao;
using fa_security_ui.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TezaUtils.Helper;

namespace fa_security_ui.Forms
{
    public partial class FrmUsuarioSucursal : Form
    {

        private static readonly ILog Logger = LogManager.GetLogger(typeof(FrmUsuarioSucursal));

        private EmpSucDao esdao;

        private UsuarioConEmpresa usuconemp;
          
        public FrmUsuarioSucursal(UsuarioConEmpresa usuconemp)
        {
            InitializeComponent();
            this.usuconemp = usuconemp;
            this.esdao = new EmpSucDao();
            RefreshData();
        }

        private void RefreshData()
        {
            InitializeListView();
            this.textBox1.Text = this.usuconemp.usuario.C15_USUARIO;
            this.textBox2.Text = this.usuconemp.usuario.DES_USUARIO;
            this.textBox3.Text = this.usuconemp.emppadre.COD_EMP_PADRE;
            this.textBox4.Text = this.usuconemp.emppadre.ROB_RAZON;

            var empsucdao = new EmpSucDao();

            var list = empsucdao.GetSucursalesAsignadaUsuarioEmpresa(this.usuconemp.usuario, this.usuconemp.emppadre);
            lvSucursal.BeginUpdate();
            lvSucursal.Items.Clear();

            var items = new ListViewItem[list.Count];
            var itemNro = 0;

            foreach (var es in list)
            {
                var item = CreateItemTrxlv(es);

                items[itemNro++] = item;
            }
            lvSucursal.Items.AddRange(items);
            lvSucursal.EndUpdate();
            toolStripStatusLabel2.Text = list.Count().ToString();
            this.statusStrip1.Refresh();
        }

        private ListViewItem CreateItemTrxlv(EmpSuc es)
        {
            string[] row = { es.NRO_EMPSUC.ToString(), es.RDE_EMPSUC_NOMBRE, es.RCD_PROVINCIA};
            ListViewItem item = new ListViewItem(row);

            item.Tag = es;
            return item;
        }

        private void InitializeListView()
        {
            lvSucursal.Columns.Clear();
            lvSucursal.Columns.Add("NRO_EMPSUC");
            lvSucursal.Columns.Add("NOMBRE");
            lvSucursal.Columns.Add("Provincia");
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                var frm = new FrmSelectSucursal(esdao.GetSucursalesNOAsignadaUsuario(usuconemp.usuario, usuconemp.emppadre), null);
                frm.ShowDialog();
                if (frm.value != null)
                {
                    var es = (EmpSuc)frm.value;
                    esdao.AgregarSucursalUsuarioEmpresa(usuconemp.usuario, usuconemp.emppadre, es);
                    MessageBox.Show("Sucursal Asignada con Exito");
                    RefreshData();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                Logger.Error(ex.StackTrace);
                throw ex;
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if ((new TestDBHelper()).TestConnection())
            {
                if (this.lvSucursal.SelectedItems.Count > 0)
                {
                    var es = (EmpSuc)(this.lvSucursal.SelectedItems[0]).Tag;
                    DialogResult result = MessageBox.Show("Desea eliminar la sucursal? " + es.NRO_EMPSUC+ " | " + es.RDE_EMPSUC_NOMBRE, "Confirmation", MessageBoxButtons.YesNoCancel);
                    if (result == DialogResult.Yes)
                    {
                        this.esdao.EliminarSucursalUsuarioEmpresa(this.usuconemp.usuario, this.usuconemp.emppadre, es);
                    }
                    RefreshData();
                    MessageBox.Show(this, "Sucursal eliminada con exito");
                }
                else
                {
                    MessageBox.Show("Debe seleccionar una sucursal para eliminar");
             
                }
            }
        }
    }
}
