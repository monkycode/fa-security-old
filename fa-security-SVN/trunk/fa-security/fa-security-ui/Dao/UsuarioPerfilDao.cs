﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TezaUtils.Dao;
using TezaUtils.Model;

namespace fa_security_ui.Dao
{
    public class UsuarioPerfilDao
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(UsuarioPerfilDao));

        public List<String> GetProgramas(Usuario usuario)
        {
            var connection = SqldbConnection.GetDbConnection();

            var list = new List<String>();


            var qry = " SELECT distinct DES_PROGRAMA from USU_PERF"
                    + " where C15_USUARIO = '" + usuario.C15_USUARIO.Trim() + "'";


            Logger.Debug(qry);

            using (IDbCommand cmd = new SqlCommand(qry, connection))
            {
                try
                {
                    using (IDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            list.Add(DBHelper.GetString(r["DES_PROGRAMA"]));
                        }
                        r.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Error(ex.StackTrace);
                    throw ex;
                }
            }
            return list;
        }


        public void ActualizarUsuarioPerfil(PerfilUsuario pu)
        {
            var cmd = new SqlCommand();
            cmd.Connection = SqldbConnection.GetDbConnection();

            cmd.CommandText = " update USU_PERF "
                            + " set EST_MENU = '" + pu.EST_MENU + "', "
                            + " EST_ALTA = '" + pu.EST_ALTA + "', "
                            + " EST_BAJA = '" + pu.EST_BAJA + "', "
                            + " EST_MODIFICA = '" + pu.EST_MODIFICA + "', "
                            + " EST_AUX1 = '" + pu.EST_AUX1 + "', "
                            + " EST_AUX2 = '" + pu.EST_AUX2 + "'"
                            + " where C15_USUARIO = '" + pu.C15_USUARIO + "'"
                            + " and NRO_ITEM  = " + pu.NRO_ITEM + ""
                            + " and DES_PROGRAMA = '" + pu.DES_PROGRAMA + "'";
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Logger.Error(cmd.CommandText);
                Logger.Error(ex.ToString());
                Logger.Error(ex.Message);
                throw ex;
            }
        }


        public List<String> GetPerfiles()
        {
            var connection = SqldbConnection.GetDbConnection();
            var list = new List<String>();
            var qry = " SELECT distinct COD_PERFIL from PERFILES";
            Logger.Debug(qry);
            using (IDbCommand cmd = new SqlCommand(qry, connection))
            {
                try
                {
                    using (IDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            list.Add(DBHelper.GetString(r["COD_PERFIL"]));
                        }
                        r.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Error(ex.StackTrace);
                    throw ex;
                }
            }
            return list;
        }


        /// <summary>
        /// Cambia el perfil de un usuario, actulizando la tabla de usuario y borrando usuario perfil y
        /// copiando en nuevo perfil. De manera transaccional
        /// Maneja la transaccion
        /// </summary>
        /// <param name="usuario">Usuario a actualizar</param>
        /// <param name="codPerfil">Nuevo perfil para el usuario</param>
        public void CambiarPerfil(Usuario usuario, String codPerfil)
        {
            SqlTransaction tx = null;
            var udao = new UsuarioABMDao();
            var pdao = new PerfilDao();
            try
            {
                SqlConnection conn = SqldbConnection.GetDbConnection();
                tx = conn.BeginTransaction();
                // borro los perfiles de usu perf 
                AplicarCambioDePerfil(usuario, codPerfil, tx, udao, pdao, conn);
                tx.Commit();
            }
            catch (Exception e)
            {
                tx.Rollback();
                Logger.Error(e.Message);
                Logger.Error(e.StackTrace);
                throw e;
            }

        }

        public void AplicarCambioDePerfil(Usuario usuario, String codPerfil, SqlTransaction tx, UsuarioABMDao udao, PerfilDao pdao, SqlConnection conn)
        {
            try
            {

                BorrarPerfilUsuario(usuario, tx, conn);
                // actualizo el codigo de perfil nuevo en el usuario
                usuario.COD_PERFIL = codPerfil;
                udao.UpdateUsuario(usuario, tx, conn);

                //me traigo los perfiles nuevos
                var list = pdao.GetPerfil(codPerfil, tx, conn);

                foreach (var p in list)
                {
                    var pu = PerfilToPerfilUsuario(usuario, p);
                    AddPerfilUsuario(pu, tx, conn);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                Logger.Error(e.StackTrace);
                throw e;
            }
        }

        private PerfilUsuario PerfilToPerfilUsuario(Usuario usuario, Perfil p)
        {
            var pu = new PerfilUsuario();
            pu.C15_USUARIO = usuario.C15_USUARIO;
            pu.DES_MENU = p.DES_MENU;
            pu.DES_PROGRAMA = p.DES_PROGRAMA;
            pu.EST_ALTA = p.EST_ALTA;
            pu.EST_AUX1 = p.EST_AUX1;
            pu.EST_AUX2 = p.EST_AUX2;
            pu.EST_BAJA = p.EST_BAJA;
            pu.EST_MENU = p.EST_MENU;
            pu.EST_MODIFICA = p.EST_MODIFICA;
            pu.NRO_ITEM = p.NRO_ITEM;
            return pu;
        }

        private void AddPerfilUsuario(PerfilUsuario pu, SqlTransaction tx, SqlConnection conn)
        {
            var cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.Transaction = tx;
            cmd.CommandText = " insert into USU_PERF (EST_MENU, EST_ALTA, EST_BAJA, EST_MODIFICA, EST_AUX1, EST_AUX2, C15_USUARIO, DES_MENU, NRO_ITEM, DES_PROGRAMA ) "
                            + " values ('" + pu.EST_MENU + "', "
                                     + " '" + pu.EST_ALTA + "', "
                                     + " '" + pu.EST_BAJA + "', "
                                     + " '" + pu.EST_MODIFICA + "', "
                                     + " '" + pu.EST_AUX1 + "', "
                                     + " '" + pu.EST_AUX2 + "', "
                                     + " '" + pu.C15_USUARIO + "',"
                                     + " '" + pu.DES_MENU + "',"
                                     + "  " + pu.NRO_ITEM + ", "
                                     + " '" + pu.DES_PROGRAMA + "')";
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Logger.Error(cmd.CommandText);
                Logger.Error(ex.ToString());
                Logger.Error(ex.Message);
                throw ex;
            }
        }

        public void BorrarPerfilUsuario(Usuario usuario, SqlTransaction tx, SqlConnection conn)
        {
            var cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.Transaction = tx;
            cmd.CommandText = " delete USU_PERF "
                            + " where LTRIM(RTRIM(C15_USUARIO)) = '" + usuario.C15_USUARIO.Trim() + "'";

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Logger.Error(cmd.CommandText);
                Logger.Error(ex.ToString());
                Logger.Error(ex.Message);
                throw ex;
            }
        }

        public void ActualizarPermisosFaltantes(Usuario usuario)
        {
            var connection = SqldbConnection.GetDbConnection();

            var list = new List<PerfilUsuario>();
            var tx = connection.BeginTransaction();
            var pdao = new PerfilDao();

            var qry = " select * from PERFILES p "
                    + " where not exists "
                    + " 	 (select pu.NRO_ITEM from USU_PERF pu "
                    + "       where pu.C15_USUARIO = '"+usuario.C15_USUARIO.Trim()+"' "
                    + " 	  and pu.NRO_ITEM = p.NRO_ITEM "
                    + "       and pu.DES_PROGRAMA = p.DES_PROGRAMA) "
                    + " and p.COD_PERFIL = '"+usuario.COD_PERFIL.Trim()+"'";


            Logger.Debug(qry);

            using (IDbCommand cmd = new SqlCommand(qry, connection, tx))
            {
                try
                {
                    using (IDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            var p = pdao.RsToPerfil(r);
                            list.Add(PerfilToPerfilUsuario(usuario, p));
                        }
                        r.Close();
                    }

                    foreach (var pu in list)
                    {
                        AddPerfilUsuario(pu, tx, connection);
                    }
                    tx.Commit();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    Logger.Error(ex.Message);
                    Logger.Error(ex.StackTrace);
                    throw ex;
                }
            }
        }


        public Int32 GetCountPermisos(String codperfil)
        {
            var connection = SqldbConnection.GetDbConnection();

            var qry = " SELECT count(1) cant from PERFILES where COD_PERFIL = '" + codperfil + "'";

            Int32 count = 0;

            Logger.Debug(qry);

            using (IDbCommand cmd = new SqlCommand(qry, connection))
            {
                try
                {
                    using (IDataReader r = cmd.ExecuteReader())
                    {
                        if (r.Read())
                        {
                            count = (DBHelper.GetInt32(r["cant"]));
                        }
                        r.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Error(ex.StackTrace);
                    throw ex;
                }
            }
            return count;
        }

        public Int32 GetCountPermisosUsuario(String c15usuario)
        {
            var connection = SqldbConnection.GetDbConnection();

            var qry = " SELECT count(1) cant from USU_PERF where C15_USUARIO = '" + c15usuario + "'";

            Int32 count = 0;

            Logger.Debug(qry);

            using (IDbCommand cmd = new SqlCommand(qry, connection))
            {
                try
                {
                    using (IDataReader r = cmd.ExecuteReader())
                    {
                        if (r.Read())
                        {
                            count = (DBHelper.GetInt32(r["cant"]));
                        }
                        r.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Error(ex.StackTrace);
                    throw ex;
                }
            }
            return count;
        }
    }
}
