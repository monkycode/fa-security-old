﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TezaUtils.Dao;
using TezaUtils.Model;

namespace fa_security_ui.Dao
{
    public class UsuarioABMDao
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(UsuarioABMDao));

        public Boolean UsuarioDisponible(String usuario)
        {

            var connection = SqldbConnection.GetDbConnection();
            
            var result = false;

            var qry = " SELECT 1 from USUARIO where LTRIM(RTRIM(C15_USUARIO)) = '"+usuario.Trim()+"'";

            Logger.Debug(qry);
            using (IDbCommand cmd = new SqlCommand(qry, connection))
            {
                try
                {
                    using (IDataReader r = cmd.ExecuteReader())
                    {
                        if (r.Read())
                        {
                            result = false; ;
                        }
                        else
                        {
                            result = true;
                        }
                        r.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Error(ex.StackTrace);
                    throw ex;
                }
            }
            return result;
        }


        public void UpdateUsuario(Usuario u, SqlTransaction tx, SqlConnection conn)
        {
            var cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.Transaction = tx;
            cmd.CommandText = " UPDATE USUARIO "
                            + " set DES_USUARIO ='" + u.DES_USUARIO + "', "
                            + " RDE_PASSWORD = '" + u.RDE_PASSWORD + "', "
                            + " EST_HABILITADO = '" + u.EST_HABILITADO + "', "
                            + " COD_PERFIL = '" + u.COD_PERFIL + "', "
                            + " FEC_ALTA = @FEC_ALTA,"
                            + " RDE_HOST_SMTP = '" + u.RDE_HOST_SMTP + "', "
                            + " RDE_LOGIN_SMTP = '" + u.RDE_LOGIN_SMTP + "', "
                            + " RDE_PASS_SMTP = '" + u.RDE_PASS_SMTP + "', "
                            + " EST_AUTORIZACION ='" + u.EST_AUTORIZACION + "', "
                            + " RDE_MAIL =' " + u.RDE_MAIL + "', "
                            + " NRO_PUERTO_SMTP ='" + u.NRO_PUERTO_SMTP + "', "
                            + " EST_SSL ='" + u.EST_SSL + "', "
                            + " EST_ENCRIPTADO ='" + u.EST_ENCRIPTADO + "'"
                            + " where C15_USUARIO = '" + u.C15_USUARIO + "'";
            try
            {
                SqlParameter param1 = new SqlParameter();
                param1.ParameterName = "@FEC_ALTA";
                param1.SqlDbType = SqlDbType.DateTime;

                param1.Value = u.FEC_ALTA;
                cmd.Parameters.Add(param1);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Logger.Error(cmd.CommandText);
                Logger.Error(ex.ToString());
                Logger.Error(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Inserta el nuevo usuario en la tabla ademas agrega el perfil
        /// Maneja la transaccion.
        /// </summary>
        /// <param name="u"></param>
        public void AddUsuario(Usuario u)
        {
            var cmd = new SqlCommand();

            var conn = SqldbConnection.GetDbConnection();
            var tx = conn.BeginTransaction();

            cmd.Connection = conn;
            cmd.Transaction = tx;
            cmd.CommandText = " insert into USUARIO (C15_USUARIO, COD_PERFIL, DES_USUARIO, EST_AUTORIZACION, EST_ENCRIPTADO, EST_HABILITADO, EST_SSL, RDE_HOST_SMTP, RDE_LOGIN_SMTP, RDE_MAIL, RDE_PASS_SMTP, RDE_PASSWORD, FEC_ALTA, NRO_PUERTO_SMTP) "
                            + " values ( '" + u.C15_USUARIO + "', "
                            + " '" + u.COD_PERFIL + "', "
                            + " '" + u.DES_USUARIO + "', "
                            + " '" + u.EST_AUTORIZACION + "', "
                            + " '" + u.EST_ENCRIPTADO + "', "
                            + " '" + u.EST_HABILITADO + "', "
                            + " '" + u.EST_SSL + "', "
                            + " '" + u.RDE_HOST_SMTP + "', "
                            + " '" + u.RDE_LOGIN_SMTP + "', "
                            + " '" + u.RDE_MAIL + "', "
                            + " '" + u.RDE_PASS_SMTP + "', "
                            + " '" + u.RDE_PASSWORD + "',  @FEC_ALTA, "
                            + " " + u.NRO_PUERTO_SMTP + ")";
            try
            {
                SqlParameter param1 = new SqlParameter();
                param1.ParameterName = "@FEC_ALTA";
                param1.SqlDbType = SqlDbType.DateTime;

                param1.Value = u.FEC_ALTA;
                cmd.Parameters.Add(param1);

                cmd.ExecuteNonQuery();

                var updao = new UsuarioPerfilDao();
                updao.AplicarCambioDePerfil(u, u.COD_PERFIL, tx, this, new PerfilDao(), conn);
                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                Logger.Error(cmd.CommandText);
                Logger.Error(ex.ToString());
                Logger.Error(ex.Message);
                throw ex;
            }
        }

        public void DeleteUsuario(Usuario u)
        {
            var cmd = new SqlCommand();

            var conn = SqldbConnection.GetDbConnection();
            var tx = conn.BeginTransaction();

            cmd.Connection = conn;
            cmd.Transaction = tx;
            cmd.CommandText = " delete USUARIO where C15_USUARIO = '" + u.C15_USUARIO + "'";

            try
            {

                var updao = new UsuarioPerfilDao();
                updao.BorrarPerfilUsuario(u, tx, conn);

                cmd.ExecuteNonQuery();

            
                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                Logger.Error(cmd.CommandText);
                Logger.Error(ex.ToString());
                Logger.Error(ex.Message);
                throw ex;
            }
        }

        public void UpdateUsuario(Usuario usuario)
        {
            var conn = SqldbConnection.GetDbConnection();
            var tx = conn.BeginTransaction();
            try
            {
                this.UpdateUsuario(usuario, tx, conn);
                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                Logger.Error(ex.ToString());
                Logger.Error(ex.Message);
                throw ex;  
            }
        }
    }
}
