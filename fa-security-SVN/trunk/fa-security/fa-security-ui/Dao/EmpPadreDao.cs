﻿using fa_security_ui.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TezaUtils.Dao;
using TezaUtils.Model;

namespace fa_security_ui.Dao
{
    public class EmpPadreDao
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(EmpPadreDao));

        public List<EmpPadre> GetEmpresasAsignadaUsuario(Usuario usuario)
        {
            var connection = SqldbConnection.GetDbConnection();

            var list = new List<EmpPadre>();


            var qry = " SELECT ep.* from EMP_PADRE ep, USU_EMP ue "
                    + " where ep.COD_EMP_PADRE = ue.COD_EMP_PADRE "
                    + " and ue.C15_USUARIO = '"+usuario.C15_USUARIO+"'";

            Logger.Debug(qry);

            using (IDbCommand cmd = new SqlCommand(qry, connection))
            {
                try
                {
                    using (IDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            list.Add(RsToEmpPadre(r));
                        }
                        r.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Error(ex.StackTrace);
                    throw ex;
                }
            }
            return list;
        }

        public List<EmpPadre> GetEmpresasNOAsignadaUsuario(Usuario usuario)
        {
            var connection = SqldbConnection.GetDbConnection();

            var list = new List<EmpPadre>();

            var qry = " SELECT ep.* from EMP_PADRE ep "
                    + " where ep.COD_EMP_PADRE not in "
                    + " (select ue.COD_EMP_PADRE "
                    + " from USU_EMP ue where ue.C15_USUARIO = '"+ usuario.C15_USUARIO.Trim() + "')";

            Logger.Debug(qry);

            using (IDbCommand cmd = new SqlCommand(qry, connection))
            {
                try
                {
                    using (IDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            list.Add(RsToEmpPadre(r));
                        }
                        r.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Error(ex.StackTrace);
                    throw ex;
                }
            }
            return list;
        }

        public void AgregarEmpPadreAUsuario(Usuario usuario, EmpPadre emppadre)
        {
            var cmd = new SqlCommand();
            cmd.Connection = SqldbConnection.GetDbConnection();

            cmd.CommandText = " INSERT USU_EMP (C15_USUARIO, COD_EMP_PADRE, EST_AUTORIZA) "
                            + " values ('" + usuario.C15_USUARIO.Trim() + "', "
                            + " '" + emppadre.COD_EMP_PADRE.Trim() + "', "
                            + " 'S') ";
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Logger.Error(cmd.CommandText);
                Logger.Error(ex.ToString());
                Logger.Error(ex.Message);
                throw ex;
            }
        }


        public void EliminarEmpPadreAUsuario(Usuario usuario, EmpPadre emppadre)
        {
            var cmd = new SqlCommand();
            cmd.Connection = SqldbConnection.GetDbConnection();

            cmd.CommandText = " delete USU_EMP "
                            + " where C15_USUARIO = '"+ usuario.C15_USUARIO.Trim() + "'"
                            + " and COD_EMP_PADRE = '" + emppadre.COD_EMP_PADRE.Trim() + "'";

            
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Logger.Error(cmd.CommandText);
                Logger.Error(ex.ToString());
                Logger.Error(ex.Message);
                throw ex;
            }
        }
        private EmpPadre RsToEmpPadre(IDataReader r)
        {
            var emppadre = new EmpPadre();
            emppadre.C15_USUARIO = DBHelper.GetString(r["C15_USUARIO"]);
            emppadre.COD_CATEGORIA = DBHelper.GetString(r["COD_CATEGORIA"]);
            emppadre.COD_EMP_PADRE = DBHelper.GetString(r["COD_EMP_PADRE"]);
            emppadre.DES_CUIT = DBHelper.GetString(r["DES_CUIT"]);
            emppadre.DES_DNRP = DBHelper.GetString(r["DES_DNRP"]);
            emppadre.DES_INGBRUTOS = DBHelper.GetString(r["DES_INGBRUTOS"]);
            emppadre.DES_JUBILACION = DBHelper.GetString(r["DES_JUBILACION"]);
            emppadre.EST_AGENTE_IB = DBHelper.GetString(r["EST_AGENTE_IB"]);
            emppadre.EST_AGENTE_IVA = DBHelper.GetString(r["EST_AGENTE_IVA"]);
            emppadre.EST_CLIPRO = DBHelper.GetString(r["EST_CLIPRO"]);
            emppadre.EST_CTAUSU = DBHelper.GetString(r["EST_CTAUSU"]);
            emppadre.EST_DESPACHO = DBHelper.GetString(r["EST_DESPACHO"]);
            emppadre.EST_FMA_COBPAG = DBHelper.GetString(r["EST_FMA_COBPAG"]);
            emppadre.EST_IMPINT = DBHelper.GetString(r["EST_IMPINT"]);
            emppadre.EST_TIPOEMP = DBHelper.GetString(r["EST_TIPOEMP"]);
            emppadre.EST_UNINEG = DBHelper.GetString(r["EST_UNINEG"]);
            emppadre.EST_VALIDAR_CUIT = DBHelper.GetInt32(r["EST_VALIDAR_CUIT"]);
            emppadre.FEC_INICIO_ACT = DBHelper.GetDateTime(r["FEC_INICIO_ACT"]);
            emppadre.FEC_USUARIO = DBHelper.GetDateTime(r["FEC_USUARIO"]);
            emppadre.IMG_LOGO = (byte[])(r["IMG_LOGO"]);
            emppadre.NUM_VER_ARCH = DBHelper.GetDecimal(r["NUM_VER_ARCH"]);
            emppadre.OBS_DOMICILIO = DBHelper.GetString(r["OBS_DOMICILIO"]);
            emppadre.RCD_TIPO_CONV = DBHelper.GetString(r["RCD_TIPO_CONV"]);
            emppadre.ROB_FANTASIA = DBHelper.GetString(r["ROB_FANTASIA"]);
            emppadre.ROB_RAZON = DBHelper.GetString(r["ROB_RAZON"]);
            emppadre.TEL_EMAIL = DBHelper.GetString(r["TEL_EMAIL"]);
            emppadre.TEL_TELFAX = DBHelper.GetString(r["TEL_TELFAX"]);

            return emppadre;
        }
    }
}
