﻿using fa_security_ui.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TezaUtils.Dao;

namespace fa_security_ui.Dao
{
    public class PerfilABMDao
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(UsuarioABMDao));


        public void AddPerfil(Perfiles p)
        {
            var cmd = new SqlCommand();

            var conn = SqldbConnection.GetDbConnection();
            var tx = conn.BeginTransaction();

            cmd.Connection = conn;
            cmd.Transaction = tx;
            cmd.CommandText = " insert into PERFILES (COD_PERFIL, NRO_ITEM, DES_PROGRAMA, DES_MENU, EST_MENU, EST_ALTA, EST_BAJA, EST_MODIFICA, EST_AUX1, EST_AUX2) "
                            + " values ( '" + p.COD_PERFIL + "', "
                            + " '" + p.NRO_ITEM + "', "
                            + " '" + p.DES_PROGRAMA + "', "
                            + " '" + p.DES_MENU + "', "
                            + " '" + p.EST_MENU + "', "
                            + " '" + p.EST_ALTA + "', "
                            + " '" + p.EST_BAJA + "', "
                            + " '" + p.EST_MODIFICA + "', "
                            + " '" + p.EST_AUX1 + "', "
                            + " '" + p.EST_AUX2 + "')";
            try
            {
               cmd.ExecuteNonQuery();
               tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                Logger.Error(cmd.CommandText);
                Logger.Error(ex.ToString());
                Logger.Error(ex.Message);
                throw ex;
            }
        }

        public void DeletePerfil(Perfiles p)
        {
            var cmd = new SqlCommand();

            var conn = SqldbConnection.GetDbConnection();
            var tx = conn.BeginTransaction();

            cmd.Connection = conn;
            cmd.Transaction = tx;
            cmd.CommandText = " delete PERFILES where COD_PERFIL = '" + p.COD_PERFIL + "'"
                              + "AND" + " NRO_ITEM = '" + p.NRO_ITEM + "'"
                              + "AND" + " DES_PROGRAMA = '" + p.DES_PROGRAMA + "' ";

            try
            {
                cmd.ExecuteNonQuery();
                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                Logger.Error(cmd.CommandText);
                Logger.Error(ex.ToString());
                Logger.Error(ex.Message);
                throw ex;
            }
        }

        public void UpdatePerfil(Perfiles p)
        {
            var conn = SqldbConnection.GetDbConnection();
            var tx = conn.BeginTransaction();
            try
            {
                this.UpdatePerfil(p, tx, conn);
                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                Logger.Error(ex.ToString());
                Logger.Error(ex.Message);
                throw ex;
            }
        }

        public void UpdatePerfil(Perfiles p, SqlTransaction tx, SqlConnection conn)
        {
            var cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.Transaction = tx;
            cmd.CommandText = " UPDATE PERFILES "
                            + " set DES_MENU ='" + p.DES_MENU + "', "
                            + " EST_MENU = '" + p.EST_MENU + "', "
                            + " EST_ALTA = '" + p.EST_ALTA + "', "
                            + " EST_BAJA = '" + p.EST_BAJA + "', "
                            + " EST_MODIFICA = '" + p.EST_MODIFICA + "', "
                            + " EST_AUX1 = '" + p.EST_AUX1 + "', "
                            + " EST_AUX2 = '" + p.EST_AUX2 + "'"
                            + " where COD_PERFIL = '" + p.COD_PERFIL + "'"
                            + "AND" + " NRO_ITEM = '" + p.NRO_ITEM + "'"
                            + "AND" + " DES_PROGRAMA = '" + p.DES_PROGRAMA + "' ";
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Logger.Error(cmd.CommandText);
                Logger.Error(ex.ToString());
                Logger.Error(ex.Message);
                throw ex;
            }
        }

    }
}
