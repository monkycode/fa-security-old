﻿using fa_security_ui.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TezaUtils.Dao;
using TezaUtils.Model;

namespace fa_security_ui.Dao
{
    public class EmpSucDao
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(EmpSucDao));

        public List<EmpSuc> GetSucursalesAsignadaUsuarioEmpresa(Usuario usuario, EmpPadre emppadre)
        {
            var connection = SqldbConnection.GetDbConnection();

            var list = new List<EmpSuc>();


            var qry = " SELECT es.* from EMP_SUC es, USUARIO_SUCURSAL us "
                    + " where es.COD_EMP_PADRE = us.COD_EMP_PADRE "
                    + " and es.NRO_EMPSUC = us.NRO_EMPSUC "
                    + " and us.C15_USUARIO = '" + usuario.C15_USUARIO + "'"
                    + " and us.COD_EMP_PADRE = '"+emppadre.COD_EMP_PADRE.Trim()+ "'";

            Logger.Debug(qry);

            using (IDbCommand cmd = new SqlCommand(qry, connection))
            {
                try
                {
                    using (IDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            list.Add(RsToEmpSuc(r));
                        }
                        r.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Error(ex.StackTrace);
                    throw ex;
                }
            }
            return list;
        }
        public void AgregarSucursalUsuarioEmpresa(Usuario usuario, EmpPadre emppadre, EmpSuc empsuc)
        {
            var cmd = new SqlCommand();
            cmd.Connection = SqldbConnection.GetDbConnection();

            cmd.CommandText = " INSERT USUARIO_SUCURSAL (C15_USUARIO, COD_EMP_PADRE, NRO_EMPSUC) "
                            + " values ('" + usuario.C15_USUARIO.Trim() + "', "
                            + " '" + emppadre.COD_EMP_PADRE.Trim() + "', "
                            + " "+empsuc.NRO_EMPSUC.ToString()+") ";
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Logger.Error(cmd.CommandText);
                Logger.Error(ex.ToString());
                Logger.Error(ex.Message);
                throw ex;
            }
        }

        public void EliminarSucursalUsuarioEmpresa(Usuario usuario, EmpPadre emppadre, EmpSuc empsuc)
        {
            var cmd = new SqlCommand();
            cmd.Connection = SqldbConnection.GetDbConnection();

            cmd.CommandText = " DELETE USUARIO_SUCURSAL "
                            + " where C15_USUARIO = '" + usuario.C15_USUARIO.Trim() + "'"
                            + " and COD_EMP_PADRE ='" + emppadre.COD_EMP_PADRE.Trim() + "'"
                            + " and NRO_EMPSUC = " +empsuc.NRO_EMPSUC.ToString();
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Logger.Error(cmd.CommandText);
                Logger.Error(ex.ToString());
                Logger.Error(ex.Message);
                throw ex;
            }
        }

        private EmpSuc RsToEmpSuc(IDataReader r)
        {
            var empsuc = new EmpSuc();
            empsuc.C15_USUARIO = DBHelper.GetString(r["C15_USUARIO"]);
            empsuc.COD_EMP_PADRE = DBHelper.GetString(r["COD_EMP_PADRE"]);
            empsuc.DES_EMPSUC_FAX = DBHelper.GetString(r["DES_EMPSUC_FAX"]);
            empsuc.DES_EMPSUC_LOCA = DBHelper.GetString(r["DES_EMPSUC_LOCA"]);
            empsuc.DES_EMPSUC_TELE = DBHelper.GetString(r["DES_EMPSUC_TELE"]);
            empsuc.EST_EMPSUC_HABIL = DBHelper.GetString(r["EST_EMPSUC_HABIL"]);
            empsuc.FEC_CAI = DBHelper.GetDateTime(r["FEC_CAI"]);
            empsuc.FEC_USUARIO = DBHelper.GetDateTime(r["FEC_USUARIO"]);
            empsuc.HAB_FAC_AUT = DBHelper.GetInt32(r["HAB_FAC_AUT"]);
            empsuc.NRO_CAI = DBHelper.GetDecimal(r["NRO_CAI"]);
            empsuc.NRO_EMPSUC = DBHelper.GetDecimal(r["NRO_EMPSUC"]);
            empsuc.RCD_PROVINCIA = DBHelper.GetString(r["RCD_PROVINCIA"]);
            empsuc.RDE_EMPSUC_CALLE = DBHelper.GetString(r["RDE_EMPSUC_CALLE"]);
            empsuc.RDE_EMPSUC_NOMBRE = DBHelper.GetString(r["RDE_EMPSUC_NOMBRE"]);
            empsuc.XCD_CP = DBHelper.GetString(r["XCD_CP"]);
            empsuc.XCD_CPN = DBHelper.GetString(r["XCD_CPN"]);
            empsuc.XDE_EMPSUC_ALTU = DBHelper.GetString(r["XDE_EMPSUC_ALTU"]);
            empsuc.XDE_EMPSUC_EMAIL = DBHelper.GetString(r["XDE_EMPSUC_EMAIL"]);
            empsuc.XDE_EMPSUC_PISO = DBHelper.GetString(r["XDE_EMPSUC_PISO"]);

            return empsuc;
        }

        public List<EmpSuc> GetSucursalesNOAsignadaUsuario(Usuario usuario, EmpPadre empPadre)
        {

            var connection = SqldbConnection.GetDbConnection();

            var list = new List<EmpSuc>();

            var qry = " SELECT es.* from EMP_SUC es "
                    + " where es.COD_EMP_PADRE = '" + empPadre.COD_EMP_PADRE.Trim() + "'"
                    + " and es.NRO_EMPSUC not in "
                    + " (select ue.NRO_EMPSUC  "
                    + " from USUARIO_SUCURSAL ue "
                    + " where ue.C15_USUARIO = '"+ usuario.C15_USUARIO.Trim() + "'"
                    + " and ue.COD_EMP_PADRE = '" + empPadre.COD_EMP_PADRE.Trim() + "')";

            Logger.Debug(qry);

            using (IDbCommand cmd = new SqlCommand(qry, connection))
            {
                try
                {
                    using (IDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            list.Add(RsToEmpSuc(r));
                        }
                        r.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Error(ex.StackTrace);
                    throw ex;
                }
            }
            return list;
        }

    }
}
