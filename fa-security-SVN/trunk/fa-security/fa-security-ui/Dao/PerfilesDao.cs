﻿using fa_security_ui.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TezaUtils.Dao;

namespace fa_security_ui.Dao
{
    public class PerfilesDao
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(PerfilesDao));

        public List<Perfiles> GetPerfiles() 
        {
            var connection = SqldbConnection.GetDbConnection();
            var list = new List<Perfiles>();
            var qry = " SELECT * from PERFILES ";

            Logger.Debug(qry);

            using (IDbCommand cmd = new SqlCommand(qry, connection))
            {
                try
                {
                    using (IDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            list.Add(RsToPerfiles(r));
                        }
                        r.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Error(ex.StackTrace);
                    throw ex;
                }
            }
            return list;
        }

        public List<String> GetProgramas()
        {
            var connection = SqldbConnection.GetDbConnection();

            var list = new List<String>();


            var qry = " SELECT distinct DES_PROGRAMA from PERFILES";


            Logger.Debug(qry);

            using (IDbCommand cmd = new SqlCommand(qry, connection))
            {
                try
                {
                    using (IDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            list.Add(DBHelper.GetString(r["DES_PROGRAMA"]));
                        }
                        r.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Error(ex.StackTrace);
                    throw ex;
                }
            }
            return list;
        }

        public List<String> GetPerfil()
        {
            var connection = SqldbConnection.GetDbConnection();

            var list = new List<String>();


            var qry = " SELECT distinct COD_PERFIL from PERFILES";


            Logger.Debug(qry);

            using (IDbCommand cmd = new SqlCommand(qry, connection))
            {
                try
                {
                    using (IDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            list.Add(DBHelper.GetString(r["COD_PERFIL"]));
                        }
                        r.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Error(ex.StackTrace);
                    throw ex;
                }
            }
            return list;
        }

        public List<String> GetMenu()
        {
            var connection = SqldbConnection.GetDbConnection();

            var list = new List<String>();


            var qry = " SELECT distinct DES_MENU from PERFILES";


            Logger.Debug(qry);

            using (IDbCommand cmd = new SqlCommand(qry, connection))
            {
                try
                {
                    using (IDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            list.Add(DBHelper.GetString(r["DES_MENU"]));
                        }
                        r.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Error(ex.StackTrace);
                    throw ex;
                }
            }
            return list;
        }

        private Perfiles RsToPerfiles(IDataReader r)
        {
            var perfiles = new Perfiles();
            perfiles.COD_PERFIL = DBHelper.GetString(r["COD_PERFIL"]);
            perfiles.NRO_ITEM = DBHelper.GetDecimal(r["NRO_ITEM"]);
            perfiles.DES_PROGRAMA = DBHelper.GetString(r["DES_PROGRAMA"]);
            perfiles.DES_MENU = DBHelper.GetString(r["DES_MENU"]);
            perfiles.EST_MENU = DBHelper.GetString(r["EST_MENU"]);
            perfiles.EST_ALTA = DBHelper.GetString(r["EST_ALTA"]);
            perfiles.EST_BAJA = DBHelper.GetString(r["EST_BAJA"]);
            perfiles.EST_MODIFICA = DBHelper.GetString(r["EST_MODIFICA"]);
            perfiles.EST_AUX1 = DBHelper.GetString(r["EST_AUX1"]);
            perfiles.EST_AUX2 = DBHelper.GetString(r["EST_AUX2"]);
            
            return perfiles;

        }
    }
}
