﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fa_security_ui.Model
{
    public class Perfiles
    {
        public String COD_PERFIL {get; set;}
        public Decimal NRO_ITEM { get; set; }
        public String DES_PROGRAMA { get; set; }
        public String DES_MENU { get; set; }
        public String EST_MENU { get; set; }
        public String EST_ALTA { get; set; }
        public String EST_BAJA { get; set; }
        public String EST_MODIFICA { get; set; }
        public String EST_AUX1 { get; set; }
        public String EST_AUX2 { get; set; }
    }
}
