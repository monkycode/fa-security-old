﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fa_security_ui.Model
{
    public class EmpPadre
    {
        public String COD_EMP_PADRE { get; set; }
        public String COD_CATEGORIA { get; set; }
        public String ROB_FANTASIA { get; set; }
        public String ROB_RAZON { get; set; }
        public String DES_CUIT { get; set; }
        public String DES_INGBRUTOS { get; set; }
        public String DES_JUBILACION { get; set; }
        public String OBS_DOMICILIO { get; set; }
        public String TEL_TELFAX { get; set; }
        public String TEL_EMAIL { get; set; }
        public Byte[] IMG_LOGO { get; set; }
        public String EST_TIPOEMP { get; set; }
        public String EST_UNINEG { get; set; }
        public String C15_USUARIO { get; set; }
        public DateTime FEC_USUARIO { get; set; }
        public String EST_AGENTE_IB { get; set; }
        public String RCD_TIPO_CONV { get; set; }
        public String EST_AGENTE_IVA { get; set; }
        public Decimal NUM_VER_ARCH { get; set; }
        public String EST_DESPACHO { get; set; }
        public String EST_CTAUSU { get; set; }
        public String EST_CLIPRO { get; set; }
        public DateTime FEC_INICIO_ACT { get; set; }
        public Int32 EST_VALIDAR_CUIT { get; set; }
        public String EST_FMA_COBPAG { get; set; }
        public String DES_DNRP { get; set; }
        public String EST_IMPINT { get; set; }

        public override String ToString()
        {
            return COD_EMP_PADRE + " | " +ROB_RAZON;
        }
    }
}
