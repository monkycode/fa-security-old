﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TezaUtils.Model;

namespace fa_security_ui.Model
{
    public class UsuarioConEmpresa
    {
        /// <summary>
        /// Usuario y la empresa para asignar sucursales
        /// </summary>
        public Usuario usuario { get; set; }
        public EmpPadre emppadre { get; set; }
    }
}
