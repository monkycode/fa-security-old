﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fa_security_ui.Model
{
    /// <summary>
    /// Clase que representa la relacion entre usuario y empresa
    /// </summary>
    public class UsuEmp
    {
        public String C15_USUARIO { get; set; }
        public String COD_EMP_PADRE { get; set; }
        public String EST_AUTORIZA { get; set; }

    }
}
