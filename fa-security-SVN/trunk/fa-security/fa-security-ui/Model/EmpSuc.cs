﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fa_security_ui.Model
{
    public class EmpSuc
    {
        public String COD_EMP_PADRE { get; set; }
        public Decimal NRO_EMPSUC { get; set; }
        public String RCD_PROVINCIA { get; set; }
        public String RDE_EMPSUC_NOMBRE { get; set; }
        public String RDE_EMPSUC_CALLE { get; set; }
        public String XDE_EMPSUC_ALTU { get; set; }
        public String XDE_EMPSUC_PISO { get; set; }
        public String DES_EMPSUC_LOCA { get; set; }
        public String XCD_CP { get; set; }
        public String XCD_CPN { get; set; }
        public String DES_EMPSUC_TELE { get; set; }
        public String DES_EMPSUC_FAX { get; set; }
        public String XDE_EMPSUC_EMAIL { get; set; }
        public String EST_EMPSUC_HABIL { get; set; }
        public String C15_USUARIO { get; set; }
        public DateTime FEC_USUARIO { get; set; }
        public Decimal NRO_CAI { get; set; }
        public DateTime FEC_CAI { get; set; }
        public Int32 HAB_FAC_AUT { get; set; }


        public override String ToString()
        {
            return NRO_EMPSUC + " | " + RDE_EMPSUC_NOMBRE;
        }
    }
}
